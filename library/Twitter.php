<?php

/**
 * Simple Library to get a twitter users feed
 *
 * <example>
 *  $obj    = new Library_Twitter(array("screen_name" => "twitterid"));
 *  $tweets = $obj->getTweets(5);
 * </example>
 *
 * @package Library_Twitter
 * @author Craig Winstanley
 * @version 0.1
 */
class Library_Twitter {
 
    /* @var string $url twitter feed url */
    private $url = 'https://api.twitter.com/1/statuses/user_timeline.xml';
    /* @var integer $user_id the users id */
    public $user_id;
    /* @var string $screen_name The twitter usser's username */
    public $screen_name;
    /* @var interger $count How many tweets to get */
    public $count = 2;
 
    /**
     * Constructor function called with an array
     * to set any parameters required on build
     *
     * @param string[] $param Array of parameters to preset
     * @return void
     * @throws Exception
     */
    public function __construct($param = array())
    {
        /* Get a list all object properties */
        $variables = array_keys(get_object_vars($this));
        /* Loop through all parameters passed in */
        foreach($param as $p => $value) {
            /* If its a property, set the value */
            if ( in_array($p, $variables)  ) {
                $this->$p = $value;
            }
            else {
                /* No property throw an error */
                throw new Exception("Unknown variable $p");
            }
        }
    }
 
    /**
     * Gets the tweets for the requested user
     *
     * @return array
     */
    public function getTweets($count = null)
    {
        $array = array();
        /* If no count passed in use the one set in the object */
        if (is_null($count)) {
            $count = $this->count;
        }
        /* Build the URL for the twitter feed */
        $url  = $this->url . "?include_entities=false&include_rts=false&screen_name=" . $this->screen_name . "&count=" . $count;
        /* Get the contents of the url */
        $data = file_get_contents($url);
        /* Parse the data */
        $xml  = simplexml_load_string($data);
        /* Parse the statuses */
        foreach ($xml->status as $status) {
            $array[] = current($status->text);
        }
        /* return the array of statuses */
        return $array;
    }
 
}
